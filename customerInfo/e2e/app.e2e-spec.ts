import { CustomerInfoPage } from './app.po';

describe('customer-info App', () => {
  let page: CustomerInfoPage;

  beforeEach(() => {
    page = new CustomerInfoPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
