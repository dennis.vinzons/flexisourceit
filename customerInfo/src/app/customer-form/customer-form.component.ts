import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Http } from '@angular/http';
@Component({
  selector: 'customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {
  posts: any[];
  private url = 'https://jsonplaceholder.typicode.com/users';
  form = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    deliveryAddress: new FormControl('', Validators.required),
    billingAddress: new FormControl('', Validators.required)
  });

  get firstName() {
    return this.form.get('firstName');
  }
  get lastName() {
    return this.form.get('lastName');
  }
  get deliveryAddress() {
    return this.form.get('deliveryAddress');
  }
  get billingAddress() {
    return this.form.get('billingAddress');
  }
  constructor(private http: Http) {
    http.get(this.url)
      .subscribe(response => {
        this.posts = response.json();
      });
  }

  createPost(input: HTMLInputElement) {
    let post = { name: input.value };
    input.value = '';
    this.http.post(this.url, JSON.stringify(post))
      .subscribe(response => {
        post['id'] = response.json().id;
        this.posts.splice(0, 0, post);
        console.log(response.json());
      });
  }
  // updatePost(post){
  //   this.http.patch(this.url, JSON.stringify({isRead: true}))
  //   .subscribe(reposnse=> {
  //     console.log();
  //   })
  // }

  ngOnInit() {
  }

}
